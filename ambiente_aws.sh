echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Instalacao de pacotes"
dnf install git unzip python3.9-pip ansible-core -y

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Install Boto3"
pip3.9 install boto3

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Install AWS Cli"
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Git Clone"
git clone https://gitlab.com/yrmenezes1/mack-iasansible-aula3.git

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Install Modulos Ansible"
ansible-galaxy collection install amazon.aws:==3.3.1 --force
ansible-galaxy collection install community.aws --force
ansible-galaxy collection install amazon.aws --force
