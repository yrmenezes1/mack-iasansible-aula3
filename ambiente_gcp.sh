echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Instalacao de pacotes"
dnf install git unzip python3.9-pip ansible-core -y

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Install bibliotecas python"
pip3.9 install boto3
pip3.9 install google-api-python-client 
pip3.9 install google-auth 
pip3.9 install google-auth-httplib2

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Install Modulos Ansible"
ansible-galaxy collection install google.cloud --force

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Git Clone"
git clone https://gitlab.com/yrmenezes1/mack-iasansible-aula3.git
